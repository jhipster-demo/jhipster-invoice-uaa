/**
 * View Models used by Spring MVC REST controllers.
 */
package com.company.invoicems.web.rest.vm;
